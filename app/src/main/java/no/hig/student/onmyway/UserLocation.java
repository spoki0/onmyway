
package no.hig.student.onmyway;

import android.location.Location;



//Class for a location with name
public class UserLocation {

    private String name;
    private double lat;
    private double lon;


    public UserLocation(){


    }

    public UserLocation(Location location){
        this.lat = location.getLatitude();
        this.lon = location.getLongitude();
    }

    public UserLocation(String name, double lat, double lon){
        this.name = name;
        this.lat = lat;
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
