package no.hig.student.onmyway;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class TrackSomeoneWindow extends FragmentActivity {

    // database return variables
    private String targetUsername;
    private Double targetLat;
    private Double targetLon;


    private EditText searchUsername;
    private String findUserInDB;
    private UserLocation thatSomeone;

    // map reference
    private GoogleMap myMap;

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_OMW = "onmyway";
    private static final String TAG_USERNAME = "username";
    private static final String TAG_LON = "lon";
    private static final String TAG_LAT = "lat";


    //New JSONparser that handles the database query
    private JSONParser jsonParser = new JSONParser();

    private static String URL_GET_LOCATION_OF = "http://www.stud.hig.no/~101545/get_username_data.php";

    // Progress Dialog
    private ProgressDialog pDialog;

    private IsInternetOn validInternet;



    //On creation
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_someone_window);

        //A text element so that the user can type in a username he want to search for
        searchUsername = (EditText) findViewById(R.id.TrackSomeone_Name);
        validInternet = new IsInternetOn(this);



        //Set up the map
        getMap();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_window, menu);
        return true;
    }


    class GetLocationFromServer extends AsyncTask<String, String, String> {

        private int success;
        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {

            //Dialog message that shows the user that something is indeed happening and it will show as long as the process is taking to complete
            super.onPreExecute();
            pDialog = new ProgressDialog(TrackSomeoneWindow.this);
            pDialog.setMessage(getString(R.string.TrackSomeone_Dialog_PreExecute));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }



        protected String doInBackground(String... params) {

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    // Check for success tag

                    try {
                        // Building Parameters
                        List<NameValuePair> params = new ArrayList<NameValuePair>();
                        params.add(new BasicNameValuePair("username", findUserInDB));

                        // getting location details by making HTTP request to the database server via php script
                        JSONObject json = jsonParser.makeHttpRequest(
                                URL_GET_LOCATION_OF, "GET", params);
                        // check your log for json response
                        Log.d("Single location Details", json.toString());
                        // json success tag
                        success = json.getInt(TAG_SUCCESS);
                        if (success == 1) {

                            // successfully received response, get the JSON Array of data
                            JSONArray locationObj = json.getJSONArray(TAG_OMW);

                            // get first product object from JSON Array
                            JSONObject location = locationObj.getJSONObject(0);
                            targetUsername = location.getString(TAG_USERNAME);
                            targetLat = location.getDouble(TAG_LAT);
                            targetLon = location.getDouble(TAG_LON);

                            //Logging to see that the right value is passed inn. For debugg purpose.
                            Log.d("Target username: ", targetUsername);
                            Log.d("Target latitude", targetLat.toString());
                            Log.d("Target longitude", targetLon.toString());


                            thatSomeone = new UserLocation(targetUsername, targetLat, targetLon);

                            myMap.addMarker(new MarkerOptions().position(new LatLng(targetLat,targetLon)).title(targetUsername));

                        } else {
                            Log.d("Error: ", "Couldn't find username");
                        }
                    } catch (JSONException e) {
                        Log.e("JSON", Log.getStackTraceString(e));
                    }


                }
            });

            return null;
        }
        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String fileURL) {
            // dismiss the dialog once got all details
            pDialog.dismiss();
            if( success == 1 ) {
                trackGPS();
            } else {
                Toast.makeText(getApplicationContext(), getString(R.string.TrackSomeone_Toast_NameNotFound), Toast.LENGTH_SHORT).show();
            }
        }
    }

    //Set up the map fragment.
    private void getMap() {
        if(myMap == null){
            myMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
                    .getMap();
        }
        if(myMap != null){
            myMap.setMyLocationEnabled(true);
        }
    }


    /**
     * function to retrieve user data from the database [username]:[latitude]:[longitude]
     * check if the user has entered an empty username, if soo a error message is showed to user.
     * if not it runs the async task to query the database server for user information and returns a JSON object wich is later parsed.
     */
    public void getLocationOfUser(View view){


        findUserInDB = String.valueOf(searchUsername.getText());


        if (!validInternet.isNetworkAvailable()){

            Toast.makeText(this, getString(R.string.Global_Toast_InternetError), Toast.LENGTH_SHORT).show();
        }
        // Check if they actually posted a name.
        else if (findUserInDB.isEmpty()) {
            //Post an error if they didn't add a name.
            Toast.makeText(this, getString(R.string.TrackSomeone_Toast_NoName), Toast.LENGTH_SHORT).show();
        } else {

            new GetLocationFromServer().execute();

        }
    }

    /**
     * Checks if username is empty if it is the user gets an error message.
     * if it passes the thest an intent to start google maps with the longitude and latidude provided by the database with that username.
     */
    public void startNavigation(View view){

        if (!validInternet.isNetworkAvailable()){

            Toast.makeText(this, getString(R.string.Global_Toast_InternetError), Toast.LENGTH_SHORT).show();
        }

        else if(thatSomeone == null){

            Toast.makeText(this, getString(R.string.TrackSomeone_Toast_UsernameUndefined), Toast.LENGTH_SHORT).show();

        } else {

            Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                    Uri.parse("http://maps.google.com/maps?daddr="+thatSomeone.getLat()+","+thatSomeone.getLon()));
            intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
            startActivity(intent);

        }

    }


    //Function for updating the map and move the orientation of the camera to the new location
    public void trackGPS() {


            //Moves the map to view the given position.
            try {
                myMap.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                        .target(new LatLng(thatSomeone.getLat(), thatSomeone.getLon()))
                        .zoom(10f)
                        .bearing(0)
                        .tilt(25)
                        .build()));
            } catch (Exception e) {

                //For when there is no valid GPS coordinates to use
                Log.e("GPS", e.getMessage());
                Toast.makeText(this, getString(R.string.Global_Toast_NoGPS), Toast.LENGTH_SHORT).show();

            }
    }
}