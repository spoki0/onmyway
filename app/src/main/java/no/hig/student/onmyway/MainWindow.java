package no.hig.student.onmyway;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.Menu;
import android.view.View;



public class MainWindow extends Activity{


    //On creation
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main_window);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_window, menu);
        return true;
    }


    //Start the activity where you update your GPS coordinates
    public void pickSomeoneUp(View view) {

        Intent intent = new Intent (this, PickSomeoneUpWindow.class);
        startActivity(intent);

    }

    //Start the activity that let you track your ride.
    public void trackSomeone(View view) {

        Intent intent = new Intent (this, TrackSomeoneWindow.class);
        startActivity(intent);
    }
}