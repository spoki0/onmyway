package no.hig.student.onmyway;

import android.app.ProgressDialog;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PickSomeoneUpWindow extends FragmentActivity
        implements GooglePlayServicesClient.ConnectionCallbacks,
        com.google.android.gms.location.LocationListener,
        GooglePlayServicesClient.OnConnectionFailedListener {

    private LocationClient myLocationClient;
    private UserLocation myUserLocation;

    // 5 Second Interval, Fastest interval being 16ms, Priority is High Accuraccy.
    private static final LocationRequest REQUEST = LocationRequest.create()
            .setInterval(300000)
            .setFastestInterval(60000)
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

    private String username;
    private EditText myUsername;
    private GoogleMap myMap;

    //Text dialog to the user so he knows something is happening. good design practice so he doesent spam the button.
    private ProgressDialog pDialog;

    private JSONParser jsonParser = new JSONParser();

    private IsInternetOn validInternett;

    // JSON Node names
    private static final String TAG_SUCCESS = "success";

    //bool used to update the database on every location update.
    private boolean Active = false;




    //On creation
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_someone_up_window);

        myUsername = (EditText) findViewById(R.id.usernameTxtField);
        validInternett = new IsInternetOn(this);

        getMap();

    }

    /**
     *     Activity's lifecycle event.
     *     onResume will be called when the Activity receives focus
     *     and is visible
     */
    @Override
    protected  void onResume(){
        super.onResume();
        wakeUpLocationClient();
        myLocationClient.connect();
    }

    /**
     *      Activity's lifecycle event.
     *      onPause will be called when activity is going into the background,
     */
    @Override
    public void onPause(){
        super.onPause();
        if(myLocationClient != null){
            myLocationClient.disconnect();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_window, menu);
        return true;


    }
    @Override
    public void onConnected(Bundle bundle) {
        myLocationClient.requestLocationUpdates(
                REQUEST,
                this); // LocationListener

    }

    @Override
    public void onLocationChanged(Location location) {
        myUserLocation = new UserLocation(location);
        trackGPS();
        if(Active) {
            addMyLocation(this.findViewById(android.R.id.content));
        }
    }

    @Override
    public void onDisconnected() {
        Toast.makeText(this, getString(R.string.MainWindow_Toast_GPSDisconnected), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Toast.makeText(this, getString(R.string.MainWindow_Toast_GPSFailure), Toast.LENGTH_SHORT).show();
    }



    public void addMyLocation(View view){


        username = myUsername.getText().toString();

        //If all requirements are met for adding location to the database, add to the database
        if (!validInternett.isNetworkAvailable()){
            Active = false;
            Toast.makeText(this, getString(R.string.Global_Toast_InternetError), Toast.LENGTH_SHORT).show();
        } else if (myUserLocation == null){
            Active = false;
            Toast.makeText(this, getString(R.string.Global_Toast_NoGPS), Toast.LENGTH_SHORT).show();
        } else if (username.isEmpty()) {
            Active = false;
            Toast.makeText(this, getString(R.string.PickSomeoneUp_Toast_NeedUsername), Toast.LENGTH_SHORT).show();
        } else {
            Active = true;
            Toast.makeText(this, getString(R.string.PickSomeoneUp_Toast_LoggingActive), Toast.LENGTH_LONG).show();
            new CreateNewLocation().execute();
        }

    }

    /**
     * Params, the type of the parameters sent to the task upon execution.
     * Progress, the type of the progress units published during the background computation.
     * Result, the type of the result of the background computation.
     */
    private class CreateNewLocation extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(PickSomeoneUpWindow.this);
            pDialog.setMessage(getString(R.string.PickSomeoneUp_Dialog_CreateMyLocation_Succesfull));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }


        protected String doInBackground(String... args) {


            Log.d("Longitude before db", String.valueOf(myUserLocation.getLon()));
            Log.d("Latitude before db", String.valueOf(myUserLocation.getLat()));
            Log.d("username", username);

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("username", username));
            params.add(new BasicNameValuePair("lat", String.valueOf(myUserLocation.getLat())));
            params.add(new BasicNameValuePair("lon", String.valueOf(myUserLocation.getLon())));



            // getting JSON Object
            // Note that create product url accepts POST method
            JSONObject json = jsonParser.makeHttpRequest("http://www.stud.hig.no/~101545/create_location.php",
                    "POST", params);

            // check log cat for response
            Log.d("Create Response", json.toString());

            // check for success tag
            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {

                    Log.d("Successfully ", "success");

                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.PickSomeoneUp_Toast_CreateFailed), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                Log.e("JSON", Log.getStackTraceString(e));
            }

            return null;
        }

        protected void onPostExecute(String fileURL) {
            // dismiss the dialog once done

            Log.d("Added to db", "db added");
            pDialog.dismiss();
        }

    }






    /**
     *      When we receive focus, we need to get back our LocationClient
     *      Creates a new LocationClient object if there is none
     */
    private void wakeUpLocationClient() {
        if(myLocationClient == null){
            //Context, Connection callbacks, ConnectionFailure Listener
            myLocationClient = new LocationClient(getApplicationContext(), this, this);
            Log.d("Test ", "true");
        }

    }


    //Set up the map fragment.
    private void getMap() {
        if(myMap == null){
            myMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
                    .getMap();
        }
        if(myMap != null){
            myMap.setMyLocationEnabled(true);
        }
    }

    //Function for updating the map and move the orientation of the camera to the new location
    public void trackGPS() {


        //Moves the map to view the given position.
        try {
            myMap.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                    .target(new LatLng(myUserLocation.getLat(), myUserLocation.getLon()))
                    .zoom(10f)
                    .bearing(0)
                    .tilt(25)
                    .build()));
        } catch (Exception e) {

            //For when there is no valid GPS coordinates to use
            Log.e("GPS", e.getMessage());
            Toast.makeText(this, getString(R.string.Global_Toast_NoGPS), Toast.LENGTH_SHORT).show();

        }
    }

}