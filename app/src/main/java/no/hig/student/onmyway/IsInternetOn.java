package no.hig.student.onmyway;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;


public class IsInternetOn {
    private Context mContext;

    public IsInternetOn(Context mContext){
        this.mContext = mContext;
    }
    public boolean isNetworkAvailable() {

        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
